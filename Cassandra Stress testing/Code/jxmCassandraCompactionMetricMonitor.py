import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.management.ObjectName as ObjectName;
import javax.management.QueryExp as QueryExp;
import java.lang.management.ManagementFactory;
import sys, cmd, socket, optparse
from urlparse import urljoin
from cmd import Cmd
import pprint
import os
import time
import sys


#### makes connection to the JMX server that cassandra started
def get_connection(host, port):
	global remote
	serviceURL = str()
	serviceURL = "service:jmx:rmi:///jndi/rmi://"
	serviceURL = serviceURL + host + ":" + str(port) + "/jmxrmi"
	url =  javax.management.remote.JMXServiceURL(serviceURL)
	connector = javax.management.remote.JMXConnectorFactory.connect(url)
	remote = connector.getMBeanServerConnection()

#### lists all the domains in the JMX 
def get_domain_list():
        domainList = []
        domainList = remote.getDomains()
        return domainList

#### prints the list of domains
def print_domain_list():
        domainList = get_domain_list()
        for element in domainList:
            print element

def get_domain_objects(domain):
        pass

##### prints all the objects that there for the corresponding domain
def print_object_list():
	domainList = get_domain_list()
	for element in domainList:
		print element
		str = element + ":*"
		for ele in remote.queryNames(ObjectName(str),None):
			print "\t\t" + ele.toString()
	return None

##### fetches and return a dictionary of objects in """org.apache.cassandra.metrics"""
def get_metric_object_list():
	dmiDomains = {'org.apache.cassandra.metrics':[]}
	metricObjectName = 'org.apache.cassandra.metrics'
	metricObject = metricObjectName + ":*"
	for ele in remote.queryNames(ObjectName(metricObject),None):
		dmiDomains[metricObjectName].append(ele.toString())
	return dmiDomains

##### prints the dictionary of objects in """org.apache.cassandra.metrics"""
def print_metric_object_list():
	domainObjectList = get_metric_object_list()
	pprint.pprint(domainObjectList)


####### fetch and return cassandra compaction metric mbean 
def get_metric_mbean_objects():
	# ## Gets all the mbeans
	# domainList = get_domain_list()
	# for element in domainList:
	# 	print element
	# 	str = element + ":*"
	# 	for ele in remote.queryMBeans(ObjectName(str),None):
	# 		print "\t\t" + ele.toString()
	dmiDomains = {'org.apache.cassandra.metrics':[]}
	metricObjectName = 'org.apache.cassandra.metrics'
	metricObject = metricObjectName + ":type=Compaction,*"
	# for printing all 
	# metricObject = metricObjectName + ":*"
	for ele in remote.queryMBeans(ObjectName(metricObject),None):
		dmiDomains[metricObjectName].append(ele.toString())
	return dmiDomains

###  print cassandra metric mbean 
def print_metric_object_list():
	mbeanObjectList = get_metric_mbean_objects()
	pprint.pprint(mbeanObjectList)

### generates the list of all the mbeans
def get_mbean_list():
	dmiDomains = {}
	domainList = get_domain_list()
	for element in domainList:
		print element
		str = element + ":*"
		for ele in remote.queryMBeans(ObjectName(str),None):
			print "\t\t" + ele.toString()

### prints the attrivute's value/data
def get_attribute(mbean,attribute):
	return remote.getAttribute(mbean, attribute)



def main():
	# print print_domain_list()
	# print print_object_list()
	# print print_metric_object_list()
	# print get_mbean_list()
	data = ','.join(['Time','BytesCompacted','CompletedTasks','PendingTasks','TotalCompactionsCount(eventsPerSec)','TotalCompactionsOneMinuteRate','TotalCompactionsMeanRate','TotalCompactionsFiveMinuteRate','TotalCompactionsFifteenMinuteRate','WriteCount','ReadCount'])
	# with open(argv[0],'a') as f:
	# 	f.write(data)
	# 	f.write('\n')
	print data

	metricBytesCompacted = 'org.apache.cassandra.metrics:type=Compaction,name=BytesCompacted'
	metricCompletedTasks = 'org.apache.cassandra.metrics:type=Compaction,name=CompletedTasks'
	metricPendingTasks = 'org.apache.cassandra.metrics:type=Compaction,name=PendingTasks'
	metricTotalCompactionsCompleted = 'org.apache.cassandra.metrics:type=Compaction,name=TotalCompactionsCompleted'
	metricWriteCount = 'org.apache.cassandra.metrics:type=Table,name=WriteLatency'
	metricReadCount = 'org.apache.cassandra.metrics:type=Table,name=ReadLatency'
	

	t_start = time.time()
	t_end = t_start + 60 * 30
	while time.time() < t_end:
		Time = str(time.time() - t_start) 
		BytesCompacted = str(get_attribute(ObjectName(metricBytesCompacted),"Count"))
		CompletedTasks = str(get_attribute(ObjectName(metricCompletedTasks),"Value"))
		PendingTasks = str(get_attribute(ObjectName(metricPendingTasks),"Value"))
		TotalCompactionsCount = str(get_attribute(ObjectName(metricTotalCompactionsCompleted),"Count"))
		TotalCompactionsOneMinuteRate = str(get_attribute(ObjectName(metricTotalCompactionsCompleted),"OneMinuteRate"))
		TotalCompactionsMeanRate = str(get_attribute(ObjectName(metricTotalCompactionsCompleted),"MeanRate"))
		TotalCompactionsFiveMinuteRate = str(get_attribute(ObjectName(metricTotalCompactionsCompleted),"FiveMinuteRate"))
		TotalCompactionsFifteenMinuteRate = str(get_attribute(ObjectName(metricTotalCompactionsCompleted),"FifteenMinuteRate"))
		WriteCount = str(get_attribute(ObjectName(metricWriteCount),"Count"))
		ReadCount = str(get_attribute(ObjectName(metricReadCount),"Count"))
		
		data = ','.join([Time,BytesCompacted,CompletedTasks,PendingTasks,TotalCompactionsCount,TotalCompactionsOneMinuteRate,TotalCompactionsMeanRate,TotalCompactionsFiveMinuteRate,TotalCompactionsFifteenMinuteRate,WriteCount,ReadCount])
		# with open(argv[0],'a') as f:
		# 	f.write(data)
		# 	f.write('\n')
		print data


		time.sleep(1)

    # do whatever you do
	### lists attributes meta data
	# metric = [metricBytesCompacted,metricCompletedTasks,metricPendingTasks,metricTotalCompactionsCompleted]
	# for m in metric:
	# 	obj = remote.getMBeanInfo(ObjectName(m))
	# 	for element in obj.getAttributes():
	# 		print element.toString() + "\n"



if __name__ == "__main__":
	HOST = 'localhost'
	PORT = 7199
	get_connection(HOST, PORT)
	main()



