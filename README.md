# Spark data transformer

This spark application transforms data using predefined transformations rules.

## Prerequisites
 * Spark 1.3.1 (https://spark.apache.org/docs/1.3.1/) installed.
 * sbt 0.13.x (http://www.scala-sbt.org/) installed.

## Build
```sh
$ sbt package
```

## Run
```sh
$ $SPARK_HOME/bin/spark-submit --executor-memory 3G --driver-memory 3G --master local[4] --jars lib/commons-csv.jar,lib/spark-csv_2.10.jar --class "MediationMain" target/scala-2.10/mediation_2.10-0.1.jar target/scala-2.10/classes/application.conf $@
```
Example provided in run-spark-job.sh

## Configuration
Application can be configured via application.conf.

Possible parameters

 * source.path - path to source data
 * source.columns - source columns structure
 * destination.path - path to destination data
 * destination.columns - destination data columns structure
 * transformations.path - path to transformations
 * lookups - lookups configuration

## Lookups configuration
One lookup entry has the following structure:

 * table - name of lookup table, can be used in transformations.cfg
 * columns - lookup columns structure
 * path - path to lookup data
 * join-type - type of join, possible values: "INNER JOIN", "LEFT JOIN"
 * source-column - column in the source table on which join
 * lookup-column - column in the lookup table on which join
 * output-column - destination column in the lookup table
 * transformation - transformation should be applied to output-column

## Transformations structure
```
<output_column>:<function>:<input_column>
or
<output_column>:lookup:<lookup_table_name>
```

