name := "mediation"

version := "1.0"

scalaVersion := "2.10.4"

resolvers += Resolver.mavenLocal

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.3.1",
  "org.apache.spark" %% "spark-sql" % "1.3.1",
  "com.typesafe" % "config" % "1.2.1",
  "commons-io" % "commons-io" % "2.4",
  "com.databricks" %% "spark-csv" % "1.0.3",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test")