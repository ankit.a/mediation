import java.io.File

import scala.collection.JavaConversions._

/**
 * Contains configuration parameters.
 */
class ApplicationConfig(path: String) {
  import com.typesafe.config.ConfigFactory

  private val fileConf = if (path.nonEmpty) ConfigFactory.parseFile(new File(path)) else ConfigFactory.load()
  private val conf = ConfigFactory.load(fileConf)

  val sourcePath = conf.getString("source.path")
  val sourceColumns = conf.getString("source.columns")
  val destinationPath = conf.getString("destination.path")
  val destinationColumns = conf.getString("destination.columns")
  val transformationsPath = conf.getString("transformations.path")
  val lookups = conf.getConfigList("lookups").toList.map(Lookup(_))
}
