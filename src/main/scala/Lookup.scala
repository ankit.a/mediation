import com.typesafe.config.Config

case class Lookup(table: String,
                  columns: String,
                  path: String,
                  joinType: String,
                  sourceColumn: String,
                  sourceTransformation: String,
                  lookupColumn: String,
                  outputColumn: String,
                  transformation: String)

object Lookup {
  def apply(config: Config): Lookup = {
    val table = config.getString("table")
    val columns = config.getString("columns")
    val path = config.getString("path")
    val joinType = config.getString("join-type")
    val sourceColumn = config.getString("source-column")
    val sourceTransformation = config.getString("source-transformation")
    val lookupColumn = config.getString("lookup-column")
    val outputColumn = config.getString("output-column")
    val transformation = config.getString("transformation")
    new Lookup(table, columns, path, joinType, sourceColumn, sourceTransformation, lookupColumn, outputColumn, transformation)
  }
}