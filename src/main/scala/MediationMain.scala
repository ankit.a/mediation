import java.io.File

import org.apache.commons.io.FileUtils
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.types.{StringType, StructField, StructType}

object MediationMain {
  // to not to create _SUCCESS file
  System.setProperty("spark.hadoop.mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")

  val conf = new SparkConf()
    .setAppName("Spark Mediation")
  //.set("spark.sql.shuffle.partitions", 200.toString)
  //.set("spark.sql.codegen", "true")
  //.set("spark.sql.autoBroadcastJoinThreshold", (10485760).toString)

  val sc = new SparkContext(conf)
  sc.hadoopConfiguration.set("fs.tachyon.impl", "tachyon.hadoop.TFS")
  val sqlContext = new org.apache.spark.sql.SQLContext(sc)

  val SourceTableName = "sourceData"
  val LookupFunctionName = "lookup"

  var applicationConfig: ApplicationConfig = null
  var lookupTables: Map[String, Map[String, String]] = null

  def main(args: Array[String]): Unit = {
    val startTime = System.currentTimeMillis()

    applicationConfig = loadConfig(args)
    lookupTables = loadLookupTablesIntoMemory

    val check1Time = System.currentTimeMillis()

    registerSourceTable()

    //registerLookupTables()

    registerTransformations()

    registerLookupTransformation()

    //val query = buildQuery()
    val query = buildQueryWithoutJoin
    val results = sqlContext.sql(query)

    val check2Time = System.currentTimeMillis()

    // save results
    val tmpOutputFolder = applicationConfig.destinationPath + ".tmp"
    results.save(tmpOutputFolder, "com.databricks.spark.csv")

    val check3Time = System.currentTimeMillis()

    mergeOutputFile(tmpOutputFolder)

    val endTime = System.currentTimeMillis()

    println("Query: " + query)
    println("Loading lookup into memory: " + (check1Time - startTime) + " ms")
    println("Running sql: " + (check2Time - check1Time) + " ms")
    println("Saving to file: " + (check3Time - check2Time) + " ms")
    println("Merging files: " + (endTime - check3Time) + " ms")
    println("Whole time: " + (endTime - startTime) + " ms")
  }

  /**
   * Invokes function with arbitrary name from Transformations object.
   */
  def call(methodName: String)(args: String): String = {
    def method = Transformations.getClass.getMethod(methodName, Seq(classOf[String]): _*)
    method.invoke(Transformations, Seq(args): _*).toString
  }

  private def loadConfig(args: Array[String]) = if (args.length > 0) new ApplicationConfig(args(0)) else new ApplicationConfig("")

  private def registerSourceTable() = {
    // create dynamic schema for source file
    val sourceSchema = StructType(
      applicationConfig.sourceColumns.split(",")
        .map(fieldName => StructField(fieldName, StringType, true)))
    // get dataframe of input data
    val sourceDF = sqlContext.load("com.databricks.spark.csv", sourceSchema, Map("path" -> applicationConfig.sourcePath))
    // register table for data
    sourceDF.registerTempTable(SourceTableName)
  }

  private def registerLookupTables() = {
    for (lookup <- applicationConfig.lookups) {
      val lookupSchema = StructType(lookup.columns.split(",").map(fieldName => StructField(fieldName, StringType, true)))
      val lookupDF = sqlContext.load("com.databricks.spark.csv", lookupSchema, Map("path" -> lookup.path))
      lookupDF.registerTempTable(lookup.table)
      sqlContext.cacheTable(lookup.table)
    }
  }

  private def loadLookupTablesIntoMemory = {
    (for {lookup <- applicationConfig.lookups
          lookupHeader = lookup.columns.split(",").toList
          lookupData = sc.textFile(lookup.path)
          lookupEntry = lookupData.map(line => {
            val lookupMap = (lookupHeader zip line.split(",")).toMap
            val lookupId = lookupMap.getOrElse(lookup.lookupColumn, "0")
            val lookupValue = lookupMap.getOrElse(lookup.outputColumn, "0")
            (lookupId, lookupValue)
          }).collectAsMap().toMap
    } yield (lookup.table, lookupEntry)).toMap
  }

  private def registerTransformations() = {
    val transformationFunctions = transformationsMap.map(x => x._2._1).toList
    val lookupFunctions = for (lookup <- applicationConfig.lookups) yield lookup.transformation
    val lookupSourceFunctions = for (lookup <- applicationConfig.lookups) yield lookup.sourceTransformation
    val distinctTransformations = (transformationFunctions ++ lookupFunctions ++ lookupSourceFunctions).distinct
    // register all transfomation' functions except lookup in sqlContext
    distinctTransformations.foreach(functionName =>
      if (!LookupFunctionName.equals(functionName))
        sqlContext.udf.register(functionName, call(functionName) _))
  }

  private def registerLookupTransformation() = sqlContext.udf.register("nlookup", Transformations.nlookup _)

  private def buildQueryWithoutJoin: String = s"SELECT $selectPart FROM $SourceTableName"

  lazy val selectPart = {
    def buildLookupSelect(lookupTable: String) = {
      applicationConfig.lookups.find(_.table == lookupTable) match {
        case Some(lookup) =>
          s"""${lookup.transformation}(nlookup("${lookup.table}",${lookup.sourceTransformation}(${lookup.sourceColumn}),"${lookup.outputColumn}"))"""
        case None => throw new IllegalArgumentException(s"Cannot find lookup $lookupTable in the config!")
      }
    }
    // transformation(nlookup(lookupTableName,sourceTransf(sourceColumn),outputCOlumn))
    def buildOrdinalSelect(functionName: String, sourceColumn: String) = s"$functionName($SourceTableName.$sourceColumn)"

    applicationConfig.destinationColumns.split(",")
      .map(destinationColumn => {
      transformationsMap.get(destinationColumn) match {
        case Some((functionName, sourceColumn)) =>
          if (LookupFunctionName.equals(functionName)) buildLookupSelect(sourceColumn) else buildOrdinalSelect(functionName, sourceColumn)
        case None => throw new IllegalArgumentException("Cannot find match for " + destinationColumn + " column!")
      }
    }).mkString(",")
  }

  private def buildQuery(): String = s"SELECT $selectQueryPart FROM $SourceTableName $joinQueryPart"

  lazy val selectQueryPart = {
    def buildLookupSelect(lookupTable: String) = {
      applicationConfig.lookups.find(_.table == lookupTable) match {
        case Some(lookup) => s"${lookup.transformation}(${lookup.table}.${lookup.outputColumn})"
        case None => throw new IllegalArgumentException(s"Cannot find lookup $lookupTable in the config!")
      }
    }
    def buildOrdinalSelect(functionName: String, sourceColumn: String) = s"$functionName($SourceTableName.$sourceColumn)"

    applicationConfig.destinationColumns.split(",")
      .map(destinationColumn => {
      transformationsMap.get(destinationColumn) match {
        case Some((functionName, sourceColumn)) =>
          if (LookupFunctionName.equals(functionName)) buildLookupSelect(sourceColumn) else buildOrdinalSelect(functionName, sourceColumn)
        case None => throw new IllegalArgumentException("Cannot find match for " + destinationColumn + " column!")
      }
    }).mkString(",")
  }

  lazy val joinQueryPart = {
    (for {lookup <- applicationConfig.lookups}
      yield s"${lookup.joinType} ${lookup.table} ON ${lookup.sourceTransformation}($SourceTableName.${lookup.sourceColumn}) = ${lookup.table}.${lookup.lookupColumn}")
      .mkString(" ")
  }

  /**
   * Map of transformations from transformations.path.
   * Each entry in the following format: outputColumnName -> (functionName, inputColumnName)
   */
  lazy val transformationsMap: Map[String, (String, String)] = {
    val regex = """(.*):(.*):(.*)""".r
    import scala.io.Source
    (for {line <- Source.fromFile(applicationConfig.transformationsPath).getLines()
          m <- regex.findFirstMatchIn(line)} yield (m.group(1), m.group(2), m.group(3)))
      .map(rec => rec._1 ->(rec._2, rec._3)).toMap
  }

  private def mergeOutputFile(outputPath: String): Unit = {
    val output = sc.textFile(outputPath)
    val mergedOutputPath = outputPath + ".1"
    output.coalesce(1).saveAsTextFile(mergedOutputPath)
    FileUtils.copyFile(new File(mergedOutputPath + "/part-00000"), new File(applicationConfig.destinationPath))
    FileUtils.deleteDirectory(new File(outputPath))
    FileUtils.deleteDirectory(new File(mergedOutputPath))
  }
}
