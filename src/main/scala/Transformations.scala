import java.text.SimpleDateFormat
import java.util.{TimeZone, Date}

/**
 * Transformations used in application.
 */
object Transformations {
  def identity(s: String) = s

  def identityIfNotNull(s: String) = {
    if (s == null) throw new NullPointerException
    else s
  }

  def identityIfNVL(s: String) = nullreplace(s, "0")

  def nullreplace(s: String, value: String) = {
    if (s == null) value
    else s
  }

  def sbs(s: String) = {
    var substring: String = null
    try {
      substring = s.substring(0, 15)
    } catch {
      case e: Exception =>
    }
    nullreplace(substring, "0")
  }

  def sbs_imei(s: String) = {
    var substring: String = null
    try {
      substring = s.substring(0, 8)
    } catch {
      case e: Exception =>
    }
    nullreplace(substring, "0")
  }

  def sbs_imsi(s: String) = {
    var substring: String = null
    try {
      substring = s.substring(0, 5)
    } catch {
      case e: Exception =>
    }
    nullreplace(substring, "0")
  }
 
 def lowercase(s: String) = {
    var lower: String = null
    try {
      lower = s.toLowerCase()
    } catch {
      case e: Exception =>
    }
    nullreplace(lower, "0")
  }
  def convertFromEpoch(s: String) = convertDoubleToFormat(s, "yyyy-MM-dd HH:mm:ss.SSS")

  def convertToYYYYMMDD(s: String) = convertDoubleToFormat(s, "yyyyMMdd")

  def convertToHHMISS(s: String) = convertDoubleToFormat(s, "HHmmss")

  private def convertLongToFormat(s: String, format: String) = {
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    dateFormat.format(new Date(s.toLong * 1000))
  }

  private def convertDoubleToFormat(s: String, format: String) = {
    val dateFormat = new SimpleDateFormat(format)
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
    dateFormat.format(new Date((s.toDouble * 1000).toLong))
  }
  def nlookup(tableName: String, sourceValue: String, outputColumn: String) = {
    MediationMain.lookupTables.get(tableName).flatMap(_.get(sourceValue)).getOrElse("<NOTFOUND>")
  }
}
