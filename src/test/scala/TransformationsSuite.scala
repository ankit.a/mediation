import org.scalatest.FunSuite

class TransformationsSuite extends FunSuite {
  import Transformations._

  test("identity should work correctly") {
    assert(identity("x") == "x")
    assert(identity("") == "")
    assert(identity(null) == null)
  }

  test("identityIfNotNull should work correctly") {
    assert(identityIfNotNull("x") == "x")
    intercept[NullPointerException] {
      identityIfNotNull(null)
    }
  }

//  test("convertFromEpoch should work correctly") {
//    assert(convertFromEpoch("0") == "1970-01-01 00:00:00")
//    assert(convertFromEpoch("1431834189") == "2015-05-17 03:43:09")
//  }

  test("convertToYYYYMMDD should work correctly") {
    assert(convertToYYYYMMDD("0") == "19700101")
    assert(convertToYYYYMMDD("1431834189") == "20150517")
  }

  test("convertToHHMISS should work correctly") {
    assert(convertToHHMISS("0") == "000000")
    assert(convertToHHMISS("1431834189") == "034309")
  }
}
